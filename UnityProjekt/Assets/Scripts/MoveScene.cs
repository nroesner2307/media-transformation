﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class MoveScene : MonoBehaviour
{
	public static MoveScene Instance { get; set;}
    [SerializeField] private string loadingLevel;
	//[SerializeField] private int Timer;


	public Image thisImage;
	private bool isInTransition;
	private float transisiton;
	private bool isShowing;
	private float duration;



	private void Awake(){
		Instance = this;
	}

	public void fade(bool showing, float duration){
		isShowing = showing;
		isInTransition = true;
		this.duration = duration;
		transisiton = (isShowing) ? 0 : 1;
	}


    void OnTriggerEnter(Collider other)
    {
		
      /*  if (other.CompareTag("Player"))
        {
			
			SceneManager.LoadScene(loadingLevel);

        }*/
		if (other.CompareTag ("Player")) {
			fade (true, 3f);
		}
    }


	private void update(){
			if (!isInTransition)
				return;

			transisiton += (isShowing) ? Time.deltaTime * (1 / duration) : -Time.deltaTime * (1 / duration);
			thisImage.color = Color.Lerp (new Color (1, 1, 1, 0), Color.white, transisiton);

			if (transisiton > 1 || transisiton < 0)
				isInTransition = false;
		}

	/*IEnumerator startLoadLevel()
	{
		yield return new WaitForSeconds (Timer);
		OnTriggerEnter ();

	}*/

	}