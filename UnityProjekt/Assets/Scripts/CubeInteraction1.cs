﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



public class CubeInteraction1 : MonoBehaviour {


	public GameObject kamera;
	Vector3 tempPos;

	public float gazeTime = 0.3f;
	private float timer;
	private bool gazedAt;



	// Use this for initialization
	void Start () {
		gazedAt = false; // beim starten des scripts wird gazedAt automatisch auf false
		//"nicht aktiviert" gesetzt" --> erst nach kollision mit einem anderen
		//Gameobject

	}

	// Update is called once per frame
	void Update () {


		//PointerClick (); // funktioniert aber ohne click 
		// wird nach start direkt ausgeführt und bei jedem frame wiederholt 


		//bedeutet: wenn gazedAt true ist, und der Wert des timers größer ist als die deklarierte gazeTime,
		//sollen die Events ausgeführt werden (es soll auf den pointerCLickHandler geachtet werden), und der 
		//timer auf 0 gesetzt werden (resettet)

		/*if (gazedAt) {
			timer += Time.deltaTime;

			//Debug.Log (timer);
			if(timer >= gazeTime) {
				ExecuteEvents.Execute (gameObject, new PointerEventData (EventSystem.current), ExecuteEvents.pointerClickHandler);
				timer = 0f;
			}

		}*/	
	}

	public void PointerEnter(){

		Debug.Log("Pointer Enter");
		gazedAt = true;

	}


	public void PointerExit(){
		Debug.Log("Pointer Exit");
		gazedAt = false;
	}


	public void PointerClick(){

		//hauptproblem: pointerclick soll auf jeden Frame möglichst resettet werden
		//--> mit update() verbinden --> wie?


		Debug.Log("Pointer Click");


		tempPos = kamera.transform.position;

		tempPos.z += 10f;
		//tempPos.y += 1.5f;

		kamera.transform.position = tempPos;
		Debug.Log (kamera.transform.position.z);
		Debug.Log (kamera.transform.position.y);	




		//kamera.transform.position = Vector3.up * Time.time;
	}



}



//kann man einfacher machen:
//1. normalen gazeinput schaffen
//2. 

//boxen bzw. portale machen gar nichts, außer durch ihren collider 
//zur nächsten szene das laden einzuleiten

//das clickevent soll die kamerafahrt einleiten
//dafür ist eine spezifikation der objekte notwendig, damit sie (im gegensatz zu möglicherweise
//anderen objekten nich verwechselt werden können (ist vielleicht (wahrscheinlich) auch irrelevant
//(lösungsansazz: vielleicht den collider von anderen objekten entfernen)



/*public class CubeInteractiontest : MonoBehaviour {

	//public GameObject kameraFahrt;
	Vector3 tempPos; 


	// Use this for initialization
	void Start () {



	}

	// Update is called once per frame
	void Update () {

		tempPos = transform.position;

		tempPos.z += 0.5f;

		transform.position = tempPos;
		Debug.Log (transform.position.z);
	}
}*/