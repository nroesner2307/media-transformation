﻿using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    public Slider HealthBar;
    public float health;
    public float MaxHealth;// 1000

	public static Timer instance = null;
	public static Timer Instance{
		get {return instance;}
	}

	void Awake(){
		if (instance != null && instance != this) {
			Destroy (this.gameObject);
			return;
		} 
		else 
		{
			instance = this;
		}
		DontDestroyOnLoad (this.gameObject);
	}

    void Start()
    {
        health = MaxHealth;
        HealthBar = GetComponent<Slider>();
        HealthBar.maxValue = MaxHealth;
        HealthBar.value = health;
    }
    void Update()
    {
        health -= 0.1f;
        HealthBar.value = health;

        if (MaxHealth <= 0)
        {
            Application.Quit();
        }
    }
}