﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextEffects : MonoBehaviour {

	public float letterTime;
	public string message;
	public Text messageTxt;

	// Use this for initialization
	void Start () 
	{
		messageTxt.GetComponent <Text> ();
		message = messageTxt.text;
		messageTxt.text = "";
		StartCoroutine (animatedText ());

	}
	IEnumerator animatedText()
	{
		foreach (char letter in message.ToCharArray()) 
		{
			messageTxt.text += letter;
			yield return 0;
			yield return new WaitForSeconds (letterTime);
		}
	}
}
